package com.example.qrexample.presentation.activity;

import com.example.qrexample.domain.Interactor;
import com.example.qrexample.domain.InteractorContract;

import io.reactivex.observers.DisposableSingleObserver;
import timber.log.Timber;

public class MainPresenter implements MainContract.Presenter {
    private MainContract.View view;
    private InteractorContract interactor;

    public MainPresenter() {
        interactor = new Interactor();
    }

    @Override
    public void init() {

    }

    @Override
    public void eventOperation(String l, String e) {
//        interactor.insertDao(l,e)
//                .subscribe(new DisposableSingleObserver<String>() {
//                    @Override
//                    public void onSuccess(String s) {
//
//                        view.addItem(s);
//                        dispose();
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        Timber.e("insertDao %s",e.getMessage());
//                    }
//                });
    }

    @Override
    public void startView(MainContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        if (view != null) view = null;
//        if (interactor != null) interactor = null;
    }
}
