package com.example.qrexample.presentation.adapter;

import android.view.View;

import com.example.qrexample.R;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

public class ViewHolderMain extends RecyclerView.ViewHolder {
    public ViewHolderMain(@NonNull View itemView) {
        super(itemView);
    }

    public  void bind (String login,String email){
        AppCompatTextView log =itemView.findViewById(R.id.textViewLog);
        AppCompatTextView em =itemView.findViewById(R.id.textViewEm);
        log.setText(login);
        em.setText(email);
    }
}
