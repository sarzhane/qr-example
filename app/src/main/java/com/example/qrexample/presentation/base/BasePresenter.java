package com.example.qrexample.presentation.base;

public interface BasePresenter<V> {
    void startView(V view);
    void detachView();
}
