package com.example.qrexample.presentation.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.qrexample.R;
import com.example.qrexample.data.model.dao.EntityModel;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class AdapterMain extends RecyclerView.Adapter<ViewHolderMain> {

    private List<EntityModel> list;

    public AdapterMain() {
        list = new ArrayList<>();
    }

    @NonNull
    @Override
    public ViewHolderMain onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolderMain(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_view, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderMain holder, int position) {
        if (list.size() != 0){
            EntityModel entityModel = list.get(position);
            holder.bind(entityModel.getLogin(), entityModel.getEmail());
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
