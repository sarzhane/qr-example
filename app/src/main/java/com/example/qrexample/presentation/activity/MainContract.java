package com.example.qrexample.presentation.activity;

import com.example.qrexample.data.model.dao.EntityModel;
import com.example.qrexample.presentation.base.BasePresenter;

import java.util.List;

public interface MainContract {
    interface View {
        void addItem(EntityModel item);
        void initListAdapter(List<EntityModel> list);
    }

    interface Presenter extends BasePresenter<View> {
        void init();
        void eventOperation(String l, String e);
    }
}
