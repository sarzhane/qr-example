package com.example.qrexample.presentation.activity;

import android.view.View;
import android.widget.TextView;

import com.example.qrexample.R;
import com.example.qrexample.data.model.dao.EntityModel;
import com.example.qrexample.presentation.base.BaseActivity;
import com.example.qrexample.presentation.base.BasePresenter;

import java.util.List;

import androidx.appcompat.widget.AppCompatButton;

public class MainActivity extends BaseActivity implements MainContract.View {
    MainContract.Presenter presenter;
    private TextView login;
    private TextView email;
    private AppCompatButton  save;


    @Override
    protected int getLayoutRes() {
        return R.layout.activity_main;
    }

    @Override
    protected void initView() {
        presenter = (MainContract.Presenter) new MainPresenter();
        login=findViewById(R.id.textViewLogin);
        email=findViewById(R.id.textViewEMail);
        save=findViewById(R.id.buttonSave);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String log = login.getText().toString();
                String em = email.getText().toString();
                presenter.eventOperation(log,em);
            }
        });

    }

    @Override
    protected void onStartView() {
        presenter.startView(this);
        presenter.init();
    }

    @Override
    protected void onDestroyView() {
        if (presenter != null) presenter = null;
    }

    @Override
    protected BasePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void addItem(EntityModel item) {

    }

    @Override
    public void initListAdapter(List<EntityModel> list) {

    }
}
