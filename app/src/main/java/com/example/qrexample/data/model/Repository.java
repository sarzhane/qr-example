package com.example.qrexample.data.model;

import com.example.qrexample.app.App;
import com.example.qrexample.data.model.dao.EntityModel;

import java.util.List;

import io.reactivex.Single;

public class Repository implements RepositoryContract {


    public Repository() {
    }


    @Override
    public long insert(EntityModel entityModel) {
        return App.local.insertEntity(entityModel);
    }

    @Override
    public Single<List<EntityModel>> qeuryList() {
        return App.local.queryEntityList();
    }

    @Override
    public Single<EntityModel> queryEntity(long id) {
        return App.local.queryEntry(id);
    }
}
