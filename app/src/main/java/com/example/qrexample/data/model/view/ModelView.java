package com.example.qrexample.data.model.view;

import java.util.Objects;

public class ModelView {

    private long id;
    private String login;
    private String email;

    public ModelView() {
    }

    public ModelView(long id, String login, String email) {
        this.id = id;
        this.login = login;
        this.email = email;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ModelView modelView = (ModelView) o;
        return id == modelView.id &&
                Objects.equals(login, modelView.login) &&
                Objects.equals(email, modelView.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, login, email);
    }
}
