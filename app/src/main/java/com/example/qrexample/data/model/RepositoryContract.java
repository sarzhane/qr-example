package com.example.qrexample.data.model;

import com.example.qrexample.data.model.dao.EntityModel;

import java.util.List;

import io.reactivex.Single;

public interface RepositoryContract {
    long insert(EntityModel entityModel);

    Single<List<EntityModel>> qeuryList();

    Single<EntityModel> queryEntity(long id);
}
