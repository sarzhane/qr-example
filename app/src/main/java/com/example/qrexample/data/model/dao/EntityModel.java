package com.example.qrexample.data.model.dao;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "entry_data")
public class EntityModel {
    @PrimaryKey(autoGenerate = true)
    private long id;
    @ColumnInfo (name = "login")
    private String login;
    @ColumnInfo (name = "email")
    private String email;

    @Ignore
    public EntityModel() {
    }

    public EntityModel(String login, String email) {
        this.login = login;
        this.email = email;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
