package com.example.qrexample.data.model;

public class QR {
    private static QR instance;

    private QR() {
    }

    public static synchronized QR getInstance() {
        if (instance == null) {
            instance = new QR();
        }
        return instance;
    }
}
