package com.example.qrexample.data.local;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Entity;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.qrexample.data.model.dao.EntityModel;

import io.reactivex.Single;

@Dao
public interface EntityDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertEntity(EntityModel entity);

    @Query("select * from entry_data")
    Single<List<EntityModel>> queryEntityList();

    @Query("SELECT * FROM entry_data WHERE id IS :id")
    Single<EntityModel> queryEntry(long id);
}
