package com.example.qrexample.data.local;

import androidx.room.Database;
import androidx.room.Entity;
import androidx.room.RoomDatabase;

import com.example.qrexample.data.model.dao.EntityModel;

@Database(entities = {EntityModel.class}, version = 1,exportSchema = false)
public abstract class EntityDatabase extends RoomDatabase {
    public abstract EntityDao getConverterDao();
}
