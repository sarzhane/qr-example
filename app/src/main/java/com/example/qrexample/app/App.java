package com.example.qrexample.app;

import android.app.Application;

import com.example.qrexample.Constant;
import com.example.qrexample.data.local.EntityDao;
import com.example.qrexample.data.local.EntityDatabase;

import androidx.room.Room;
import timber.log.Timber;

public class App extends Application {
    public static EntityDao local;
    @Override
    public void onCreate() {
        super.onCreate();
        Timber.plant(new Timber.DebugTree());
        //Converter.getInstance();
        local = provideLocaleDao(provideRoomDataBase());
    }

    private EntityDatabase provideRoomDataBase() {
        return Room.databaseBuilder(
                this, EntityDatabase.class, Constant.NAME_DAO)
                .allowMainThreadQueries()
                .build();
    }

    private EntityDao provideLocaleDao(EntityDatabase dao) {
        return dao.getConverterDao();
    }
}
