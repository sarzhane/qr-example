package com.example.qrexample.domain;

import java.util.List;

import io.reactivex.Single;

public interface InteractorContract {
    Single<String> insertDao(String log, String email);

    Single<List<String>> getListDao();

}
